use js_sys::*;
use wasm_bindgen::{prelude::*, JsCast};

#[wasm_bindgen]
#[derive(Debug, Clone)]
pub struct Ensemble {
    ensemble: ljf_lib::Ensemble,
}

#[wasm_bindgen]
impl Ensemble {
    pub fn new(
        n_particles: usize,
        side_len: f64,
        buffer: f64,
        mass: f64,
        temperature: f64,
    ) -> Ensemble {
        Ensemble {
            ensemble: ljf_lib::Ensemble::new(n_particles, side_len, buffer, mass, temperature),
        }
    }

    pub fn draw(&self, canvas: JsValue, ctx: JsValue, options: JsValue) -> Result<(), JsValue> {
        let canvas = match canvas.dyn_into::<web_sys::HtmlCanvasElement>() {
            Ok(c) => c,
            Err(_) => return Err(JsValue::from_str("Error: expected HtmlCanvasElement")),
        };

        let ctx = match ctx.dyn_into::<web_sys::CanvasRenderingContext2d>() {
            Ok(ctx) => ctx,
            Err(_) => {
                return Err(JsValue::from_str(
                    "Error: expected CanvasRenderingContext2d",
                ))
            }
        };

        let draw_scale = js_obj_get_f64(&options, "draw_scale")?;
        let draw_bonds = js_obj_get_bool(&options, "draw_bonds")?;
        let draw_velocities = js_obj_get_bool(&options, "draw_velocities")?;
        let velocity_draw_scale = js_obj_get_f64(&options, "velocity_draw_scale")?;
        let fps = js_obj_get_f64(&options, "fps")?;

        ctx.set_fill_style(&JsValue::from_str("black"));
        for p in self.ensemble.get_particles() {
            ctx.begin_path();
            ctx.arc(
                p.r.x * draw_scale,
                canvas.height() as f64 - p.r.y * draw_scale,
                5.0,
                0.0,
                2.0 * std::f64::consts::PI,
            )?;
            ctx.close_path();
            ctx.fill();
        }

        if draw_bonds {
            ctx.set_stroke_style(&JsValue::from_str("black"));
            for i in 0..self.ensemble.get_num_particles() {
                let p1 = &self.ensemble.get_particles()[i];
                for partition_idx in self
                    .ensemble
                    .get_surrounding_partitions_indexes(self.ensemble.get_partition_idx(p1.r))
                {
                    for index_inside_partition in 0..self.ensemble.partitions[partition_idx].len() {
                        let j = self.ensemble.partitions[partition_idx][index_inside_partition];
                        if j > i {
                            let p2 = &self.ensemble.get_particles()[j];
                            let dist = (p1.r - p2.r).len_square();
                            if dist < self.ensemble.max_bond_distance().powi(2)
                                && dist > self.ensemble.min_bond_distance().powi(2)
                            {
                                ctx.begin_path();
                                ctx.move_to(
                                    p1.r.x * draw_scale,
                                    canvas.height() as f64 - p1.r.y * draw_scale,
                                );
                                ctx.line_to(
                                    p2.r.x * draw_scale,
                                    canvas.height() as f64 - p2.r.y * draw_scale,
                                );
                                ctx.close_path();
                                ctx.stroke();
                            }
                        }
                    }
                }
            }
        }

        ctx.set_fill_style(&JsValue::from_str("black"));
        ctx.set_text_baseline("top");
        ctx.set_text_align("left");
        ctx.set_font("16px sans-serif");
        ctx.fill_text(
            &format!("Temperature: {:.3}", self.ensemble.temperature),
            10.0,
            10.0,
        )?;
        let size = ctx.measure_text("Temperature")?;
        ctx.fill_text(
            &format!("Pressure: {:.3}", self.ensemble.pressure),
            10.0,
            10.0 + 5.0 + size.actual_bounding_box_descent(),
        )?;
        ctx.set_text_align("right");
        ctx.fill_text(
            &format!("FPS: {:.0}", fps),
            canvas.width() as f64 - 10.0,
            10.0,
        )?;

        if draw_velocities {
            ctx.set_stroke_style(&JsValue::from_str("green"));
            for p in self.ensemble.get_particles() {
                ctx.begin_path();
                ctx.move_to(
                    p.r.x * draw_scale,
                    canvas.height() as f64 - p.r.y * draw_scale,
                );
                ctx.line_to(
                    p.r.x * draw_scale + p.v.x * velocity_draw_scale,
                    canvas.height() as f64 - (p.r.y * draw_scale + p.v.y * velocity_draw_scale),
                );
                ctx.close_path();
                ctx.stroke();
            }
        }

        Ok(())
    }

    pub fn get_particles_ptr(&self) -> *const ljf_lib::Particle {
        self.ensemble.get_particles_ptr()
    }

    pub fn get_num_particles(&self) -> usize {
        self.ensemble.get_num_particles()
    }

    pub fn cutoff(&self) -> f64 {
        self.ensemble.cutoff()
    }

    pub fn periodic(&self) -> bool {
        self.ensemble.periodic
    }

    pub fn set_periodic(&mut self, p: bool) {
        self.ensemble.periodic = p;
    }

    pub fn total_mass(&self) -> f64 {
        self.ensemble.total_mass()
    }

    pub fn volume(&self) -> f64 {
        self.ensemble.volume()
    }

    pub fn density(&self) -> f64 {
        self.ensemble.density()
    }

    pub fn rest_bond_distance(&self) -> f64 {
        self.ensemble.rest_bond_distance()
    }

    pub fn min_bond_distance(&self) -> f64 {
        self.ensemble.min_bond_distance()
    }

    pub fn max_bond_distance(&self) -> f64 {
        self.ensemble.max_bond_distance()
    }

    pub fn step(&mut self, dt: f64) {
        self.ensemble.step(dt);
    }

    pub fn temperature(&self) -> f64 {
        self.ensemble.temperature
    }

    pub fn pressure(&self) -> f64 {
        self.ensemble.pressure
    }

    pub fn side_len(&self) -> f64 {
        self.ensemble.side_len
    }

    pub fn set_sigma(&mut self, sigma: f64) {
        self.ensemble.sigma = sigma;
    }

    pub fn set_epsilon(&mut self, epsilon: f64) {
        self.ensemble.epsilon = epsilon;
    }
}

fn js_obj_get_prop(obj: &JsValue, prop: &str) -> Result<JsValue, JsValue> {
    let js_prop = JsValue::from_str(prop);
    if js_sys::Reflect::has(obj, &js_prop)? {
        js_sys::Reflect::get(obj, &js_prop)
    } else {
        Err(JsValue::from_str(&format!(
            "Error: expected object to have property `{}`",
            prop
        )))
    }
}

fn js_obj_get_f64(obj: &JsValue, prop: &str) -> Result<f64, JsValue> {
    if let Some(val) = js_obj_get_prop(obj, prop)?.as_f64() {
        Ok(val)
    } else {
        Err(JsValue::from_str(&format!(
            "Error: expected object property `{}` to be a number",
            prop
        )))
    }
}

fn js_obj_get_bool(obj: &JsValue, prop: &str) -> Result<bool, JsValue> {
    if let Some(val) = js_obj_get_prop(obj, prop)?.as_bool() {
        Ok(val)
    } else {
        Err(JsValue::from_str(&format!(
            "Error: expected object property `{}` to be a number",
            prop
        )))
    }
}

#[wasm_bindgen(start)]
pub fn setup() {
    #[cfg(feature = "console_error_panic_hook")]
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
    ljf_lib::rand::xorshiro256::seed(Date::now().to_bits());
}
