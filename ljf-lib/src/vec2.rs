use std::ops::*;

#[derive(Clone, Copy, Debug)]
pub struct Vec2 {
    pub x: f64,
    pub y: f64,
}

impl Vec2 {
    pub fn at<T, U>(x: T, y: U) -> Vec2
    where
        T: Into<f64>,
        U: Into<f64>,
    {
        Vec2 {
            x: x.into(),
            y: y.into(),
        }
    }

    pub fn zero() -> Vec2 {
        Vec2::at(0, 0)
    }

    pub fn len(&self) -> f64 {
        self.len_square().sqrt()
    }

    pub fn len_square(&self) -> f64 {
        self.x * self.x + self.y * self.y
    }
}

impl Add for Vec2 {
    type Output = Vec2;

    fn add(self, other: Vec2) -> Vec2 {
        Vec2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl AddAssign for Vec2 {
    fn add_assign(&mut self, other: Vec2) {
        self.x += other.x;
        self.y += other.y;
    }
}

impl Sub for Vec2 {
    type Output = Vec2;

    fn sub(self, other: Vec2) -> Vec2 {
        Vec2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl SubAssign for Vec2 {
    fn sub_assign(&mut self, other: Vec2) {
        self.x -= other.x;
        self.y -= other.y;
    }
}

impl Mul for Vec2 {
    type Output = f64;

    fn mul(self, other: Vec2) -> f64 {
        self.x * other.x + self.y * other.y
    }
}

impl<T> Mul<T> for Vec2
where
    T: Into<f64>,
{
    type Output = Vec2;

    fn mul(self, value: T) -> Vec2 {
        let scale = value.into();

        Vec2 {
            x: self.x * scale,
            y: self.y * scale,
        }
    }
}

impl<T> MulAssign<T> for Vec2
where
    T: Into<f64>,
{
    fn mul_assign(&mut self, value: T) {
        let scale = value.into();

        self.x *= scale;
        self.y *= scale;
    }
}

impl<T> Div<T> for Vec2
where
    T: Into<f64>,
{
    type Output = Vec2;

    fn div(self, value: T) -> Vec2 {
        let scale = value.into();

        Vec2 {
            x: self.x / scale,
            y: self.y / scale,
        }
    }
}

impl<T> DivAssign<T> for Vec2
where
    T: Into<f64>,
{
    fn div_assign(&mut self, value: T) {
        let scale = value.into();

        self.x /= scale;
        self.y /= scale;
    }
}

impl Neg for Vec2 {
    type Output = Vec2;

    fn neg(self) -> Vec2 {
        Vec2 {
            x: -self.x,
            y: -self.y,
        }
    }
}
