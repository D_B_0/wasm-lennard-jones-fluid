#[cfg(test)]
mod test;

use std::num::Wrapping;
static mut STATE: [Wrapping<u64>; 4] = [
    std::num::Wrapping(0),
    std::num::Wrapping(0),
    std::num::Wrapping(0),
    std::num::Wrapping(0),
];

fn rotl(x: Wrapping<u64>, k: u8) -> Wrapping<u64> {
    (x << k as usize) | (x >> (64 - k) as usize)
}

pub fn next() -> u64 {
    if unsafe { STATE[0].0 == 0 && STATE[1].0 == 0 && STATE[2].0 == 0 && STATE[3].0 == 0 } {
        panic!("xorshiro256 invoked with seed 0")
    }

    unsafe {
        let result = rotl(STATE[0] + STATE[3], 23) + STATE[0];

        let t = STATE[1] << 17;

        STATE[2] ^= STATE[0];
        STATE[3] ^= STATE[1];
        STATE[1] ^= STATE[2];
        STATE[0] ^= STATE[3];

        STATE[2] ^= t;

        STATE[3] = rotl(STATE[3], 45);

        result.0
    }
}

pub fn next_float() -> f64 {
    // generating a random float by using the upper 52 bits of
    // the u64 as the f64 mantissa, with an exponent of 2^-1.
    // this produces a number in the range [0.5, 1.0]
    let res = f64::from_bits(0x3fe << 52 | next() >> 12);
    // whe then transform the range into [0.0, 1.0]
    // i had hoped to find a method to arrive at this range
    // by using only bit manipulation techniques,
    // but this is the besst i was able to do
    (res - 0.5) * 2.0
}

/// This is the jump function for the generator. It is equivalent
/// to 2^128 calls to next(); it can be used to generate 2^128
/// non-overlapping subsequences for parallel computations.
pub fn jump() {
    static JUMP: [u64; 4] = [
        0x180ec6d33cfd0aba,
        0xd5a61266f0c9392c,
        0xa9582618e03fc9aa,
        0x39abdc4529b1661c,
    ];

    let mut s0 = Wrapping(0);
    let mut s1 = Wrapping(0);
    let mut s2 = Wrapping(0);
    let mut s3 = Wrapping(0);
    for i in 0..4 {
        for b in 0..64 {
            if (JUMP[i] & 1_u64 << b) != 0 {
                unsafe {
                    s0 ^= STATE[0];
                    s1 ^= STATE[1];
                    s2 ^= STATE[2];
                    s3 ^= STATE[3];
                }
            }
            next();
        }
    }
    unsafe {
        STATE[0] = s0;
        STATE[1] = s1;
        STATE[2] = s2;
        STATE[3] = s3;
    }
}

pub fn seed(seed: u64) {
    use super::splitmix;

    let prev_seed = splitmix::get_seed();
    splitmix::seed(seed);
    unsafe {
        STATE = [
            Wrapping(splitmix::next()),
            Wrapping(splitmix::next()),
            Wrapping(splitmix::next()),
            Wrapping(splitmix::next()),
        ]
    }
    splitmix::seed(prev_seed);
}
