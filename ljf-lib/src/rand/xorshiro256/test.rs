use super::*;

#[test]
#[should_panic]
fn panic_when_not_initialized() {
    unsafe {
        STATE[0] = Wrapping(0);
        STATE[1] = Wrapping(0);
        STATE[2] = Wrapping(0);
        STATE[3] = Wrapping(0);
    }
    next();
}

#[test]
fn seeded() {
    seed(100);
    assert_eq!(next(), 16200148097352791549_u64);
    assert_eq!(next(), 16785171618027694926_u64);
    assert_eq!(next(), 15341217898654479309_u64);
    assert_eq!(next(), 6357779920452276603_u64);
    assert_eq!(next(), 16218729523867403097_u64);
    assert_eq!(next(), 5738169174581742609_u64);
    assert_eq!(next(), 4202997657424062577_u64);
    assert_eq!(next(), 10837015921197868186_u64);
    assert_eq!(next(), 3888285286108656837_u64);
    assert_eq!(next(), 5377335802741679418_u64);
}

#[test]
fn float_should_be_in_0_to_1_range() {
    seed(12345);
    for _ in 0..10000 {
        let f = next_float();
        assert!(f >= 0.0);
        assert!(f <= 1.0);
    }
}
