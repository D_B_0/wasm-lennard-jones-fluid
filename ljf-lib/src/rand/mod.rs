/// https://prng.di.unimi.it/
///
/// https://prng.di.unimi.it/xoshiro256plusplus.c
///
/// This is `xoshiro256++ 1.0`, one of our all-purpose, rock-solid generators.
/// It has excellent (sub-ns) speed, a state (256 bits) that is large
/// enough for any parallel application, and it passes all tests we are
/// aware of.
/// For generating just floating-point numbers, `xoshiro256+` is even faster.
/// The state must be seeded so that it is not everywhere zero. If you have
/// a 64-bit seed, we suggest to seed a `splitmix64` generator and use its
/// output to fill s.
pub mod xorshiro256;

/// https://xorshift.di.unimi.it/splitmix64.c
pub mod splitmix;

pub fn rand_norm() -> f64 {
    (-2. * (1. - xorshiro256::next_float()).log(std::f64::consts::E)).sqrt()
        * (2. * std::f64::consts::PI * xorshiro256::next_float()).cos()
}

use crate::vec2::Vec2;

pub fn get_random_unit_vector(count: usize) -> Vec<Vec2> {
    let mut randos = Vec::with_capacity(count);
    for _ in 0..(count) {
        randos.push([rand_norm(), rand_norm(), rand_norm()]);
    }
    let norms: Vec<f64> = randos
        .iter()
        .map(|v| (v[0] * v[0] + v[1] * v[1] + v[2] * v[2]).sqrt())
        .collect();
    let raw: Vec<[f64; 2]> = randos
        .iter()
        .enumerate()
        .map(|(i, v)| [v[0] / norms[i], v[1] / norms[i]])
        .collect();
    let out_norms: Vec<f64> = raw
        .iter()
        .map(|v| (v[0] * v[0] + v[1] * v[1]).sqrt())
        .collect();
    raw.iter()
        .enumerate()
        .map(|(i, v)| Vec2::at(v[0] / out_norms[i], v[1] / out_norms[i]))
        .collect()
}
