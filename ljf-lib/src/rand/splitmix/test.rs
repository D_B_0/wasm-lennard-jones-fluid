use super::*;

#[test]
#[should_panic]
fn panic_when_not_initialized() {
    next();
}

#[test]
fn seeded() {
    seed(100);
    assert_eq!(next(), 2532601429470541124_u64);
    assert_eq!(next(), 269152572843532260_u64);
    assert_eq!(next(), 4491231873834608077_u64);
    assert_eq!(next(), 4673566422923057776_u64);
    assert_eq!(next(), 12322687030710318903_u64);
    assert_eq!(next(), 254950797228887656_u64);
    assert_eq!(next(), 12948712032028107689_u64);
    assert_eq!(next(), 15274025792868724242_u64);
    assert_eq!(next(), 2151863190153419181_u64);
    assert_eq!(next(), 10310841008975489961_u64);
}
