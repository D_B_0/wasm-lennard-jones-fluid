#[cfg(test)]
mod test;

use std::num::Wrapping;
static mut STATE: Wrapping<u64> = Wrapping(0);

pub fn next() -> u64 {
    if unsafe { STATE.0 } == 0 {
        panic!("splitmix invoked with seed 0")
    }

    let mut z = unsafe {
        STATE += 0x9e3779b97f4a7c15;
        STATE
    };
    z = (z ^ (z >> 30)) * Wrapping(0xbf58476d1ce4e5b9);
    z = (z ^ (z >> 27)) * Wrapping(0x94d049bb133111eb);
    (z ^ (z >> 31)).0
}

pub fn seed(seed: u64) {
    unsafe { STATE = std::num::Wrapping(seed) }
}

pub fn get_seed() -> u64 {
    unsafe { STATE.0 }
}
