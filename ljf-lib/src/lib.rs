#![allow(dead_code, non_snake_case)]

mod vec2;
use vec2::Vec2;
pub mod rand;
use rand::get_random_unit_vector;

#[derive(Debug, Clone, Copy)]
pub struct Particle {
    /// Position in 2D space
    pub r: Vec2,
    /// Velocity
    pub v: Vec2,
    /// History of accellerations (ordered from most recent to least recent)
    pub a: [Vec2; 3],
    /// Mass
    pub m: f64,
}

impl Particle {
    pub fn new(x: f64, y: f64) -> Self {
        Self {
            r: Vec2::at(x, y),
            v: Vec2::zero(),
            a: [Vec2::zero(); 3],
            m: 1.0,
        }
    }

    pub fn as_string(&self) -> String {
        format!("{:?}", self)
    }

    pub fn as_string_pretty(&self) -> String {
        format!("{:#?}", self)
    }
}

#[derive(Debug, Clone)]
pub struct Ensemble {
    pub n_particles: usize,
    pub particles: Vec<Particle>,
    pub epsilon: f64,
    pub sigma: f64,
    pub temperature: f64,
    pub cut_factor: f64,
    pub periodic: bool,
    pub side_len: f64,
    inst_pressures: Vec<f64>,
    pub pressure: f64,
    pub partitions: Vec<Vec<usize>>,
}

impl Ensemble {
    pub fn new(
        n_particles: usize,
        side_len: f64,
        buffer: f64,
        mass: f64,
        temperature: f64,
    ) -> Ensemble {
        let particles = get_points_on_square_grid(n_particles, side_len, buffer)
            .iter()
            .map(|&pos| Particle {
                r: pos,
                v: Vec2::zero(),
                a: [Vec2::zero(); 3],
                m: mass,
            })
            .collect();
        let mut e = Ensemble {
            n_particles,
            particles,
            epsilon: 1.0,
            sigma: 1.0,
            temperature,
            cut_factor: 3.0,
            periodic: true,
            side_len,
            inst_pressures: Vec::new(),
            pressure: 0.0,
            partitions: Vec::new(),
        };
        e.initialize_velocities();
        let partition_count = e.partition_count_per_side() * e.partition_count_per_side();

        e.partitions = vec![Vec::new(); partition_count];
        e.update_partitioning();
        e
    }

    pub fn get_particles<'a>(&'a self) -> &'a Vec<Particle> {
        &self.particles
    }

    pub fn get_particles_ptr(&self) -> *const Particle {
        self.particles.as_ptr()
    }

    pub fn get_num_particles(&self) -> usize {
        self.n_particles
    }

    pub fn cutoff(&self) -> f64 {
        self.sigma * self.cut_factor
    }

    pub fn total_mass(&self) -> f64 {
        self.particles.iter().fold(0.0, |accum, p| accum + p.m)
    }

    pub fn volume(&self) -> f64 {
        self.side_len * self.side_len
    }

    pub fn density(&self) -> f64 {
        self.total_mass() / self.volume()
    }

    pub fn rest_bond_distance(&self) -> f64 {
        2.0_f64.powf(1.0 / 6.0) * self.sigma
    }

    pub fn min_bond_distance(&self) -> f64 {
        // potential_energy(dist) = (3 * epsilon)/4
        // (2.0_f64.powf(1.0 / 3.0) / 3.0_f64.powf(1.0 / 6.0)) * self.sigma

        // potential_energy(dist) = epsilon/2
        (2.0 * (2.0 - 2.0_f64.sqrt())).powf(1.0 / 6.0)
    }

    pub fn max_bond_distance(&self) -> f64 {
        // potential_energy(dist) = (3 * epsilon)/4
        // 2.0_f64.powf(1.0 / 3.0) * self.sigma

        // potential_energy(dist) = epsilon/2
        (2.0 * (2.0 + 2.0_f64.sqrt())).powf(1.0 / 6.0)
    }

    pub fn step(&mut self, dt: f64) {
        for p in self.particles.iter_mut() {
            // Beeman algorithm
            // https://www.compadre.org/PICUP/resources/Numerical-Integration/
            p.r += p.v * dt + (p.a[0] * 4.0 - p.a[1]) * (1.0 / 6.0) * dt * dt;
        }

        if self.periodic {
            self.update_periodic_positions();
        } else {
            self.update_non_periodic_positions_and_velocities();
        }

        self.update_partitioning();

        self.update_accelleration_lj();

        for i in 0..self.particles.len() {
            let p = self.particles[i];
            // Beeman algorithm
            self.particles[i].v += (p.a[0] * 2.0 + p.a[1] * 5.0 - p.a[2]) * (1.0 / 6.0) * dt;
        }

        self.pressure = self.inst_pressures.iter().fold(0.0, |acc, p| acc + p)
            / self.inst_pressures.len() as f64;
        self.inst_pressures.clear();
    }

    fn initialize_velocities(&mut self) {
        let rand_vecs = get_random_unit_vector(self.n_particles);
        let mut v_sum = Vec2::zero();
        let vel_mags: Vec<f64> = self
            .particles
            .iter()
            .map(|&p| (2. * self.temperature / p.m))
            .collect();

        for (i, p) in self.particles.iter_mut().enumerate() {
            p.v = rand_vecs[i] * vel_mags[i];
            v_sum += p.v;
        }
        let v_centroid = v_sum / self.n_particles as f64;
        for p in self.particles.iter_mut() {
            p.v -= v_centroid;
        }
    }

    fn update_periodic_positions(&mut self) {
        for p in self.particles.iter_mut() {
            if p.r.x > self.side_len {
                p.r.x %= self.side_len;
            }
            if p.r.x < 0.0 {
                p.r.x += self.side_len;
            }

            if p.r.y > self.side_len {
                p.r.y %= self.side_len;
            }
            if p.r.y < 0.0 {
                p.r.y += self.side_len;
            }
        }
    }

    fn update_non_periodic_positions_and_velocities(&mut self) {
        for p in self.particles.iter_mut() {
            if p.r.x > self.side_len {
                p.r.x = self.side_len - (p.r.x - self.side_len);
                p.v.x = -p.v.x;
            }
            if p.r.x < 0.0 {
                p.r.x = -p.r.x;
                p.v.x = -p.v.x;
            }

            if p.r.y > self.side_len {
                p.r.y = self.side_len - (p.r.y - self.side_len);
                p.v.y = -p.v.y;
            }
            if p.r.y < 0.0 {
                p.r.y = -p.r.y;
                p.v.y = -p.v.y;
            }
        }
    }

    fn update_accelleration_lj(&mut self) {
        let cutoff_square = self.cutoff() * self.cutoff();
        let mut virial_sum = 0.0;
        let mut vv = 0.0;

        for p in self.particles.iter_mut() {
            p.a[2] = p.a[1];
            p.a[1] = p.a[0];
            p.a[0] = Vec2::zero();
        }
        for i in 0..self.particles.len() {
            let p_i = self.particles[i];
            vv += p_i.v * p_i.v * p_i.m;
            for partition_idx in
                self.get_surrounding_partitions_indexes(self.get_partition_idx(p_i.r))
            {
                for index_inside_partition in 0..self.partitions[partition_idx].len() {
                    let j = self.partitions[partition_idx][index_inside_partition];
                    if j > i {
                        let r_ij = self.connecting_vec(i, j);
                        let dist_sq = r_ij * r_ij;

                        if dist_sq < cutoff_square {
                            self.add_lj_pot_force(i, j);
                            virial_sum += r_ij * p_i.a[0];
                        }
                    }
                }
            }
        }
        self.temperature = vv / (2.0 * self.n_particles as f64);
        let (V, T, rho) = (self.volume(), self.temperature, self.density());
        let P = rho * T + 1.0 / 2.0 * virial_sum / V;
        self.inst_pressures.push(P);
    }

    fn add_lj_pot_force(&mut self, i: usize, j: usize) {
        let p_i = self.particles[i];
        let p_j = self.particles[j];
        let r_ij = self.connecting_vec(i, j);
        let r = r_ij.len();

        let sig6 = self.sigma.powi(6);
        let sig12 = sig6 * sig6;

        let r7 = r.powi(7);
        let r13 = r.powi(13);

        let force = 48.0 * self.epsilon * (sig12 / r13 - 0.5 * sig6 / r7);
        self.particles[i].a[0] -= (r_ij / r) * force / p_i.m;
        self.particles[j].a[0] += (r_ij / r) * force / p_j.m;
    }

    fn connecting_vec(&self, i: usize, j: usize) -> Vec2 {
        if self.periodic {
            periodic_vector_correction(self.particles[j].r - self.particles[i].r, self.side_len)
        } else {
            self.particles[j].r - self.particles[i].r
        }
    }

    fn partition_side_length(&self) -> f64 {
        self.cutoff()
    }

    fn partition_count_per_side(&self) -> usize {
        (self.side_len / self.partition_side_length()).ceil() as usize
    }

    fn update_partitioning(&mut self) {
        for partition in self.partitions.iter_mut() {
            partition.clear();
        }

        for i in 0..self.particles.len() {
            let idx = self.get_partition_idx(self.particles[i].r);
            self.partitions[idx].push(i);
        }
    }

    pub fn get_partition_idx(&self, pos: Vec2) -> usize {
        if pos.x < 0.0 || pos.x > self.side_len || pos.y < 0.0 || pos.y > self.side_len {
            panic!("`get_partition_idx` invoked with position outside of ensemble bounding box");
        }
        let x = (pos.x / self.partition_side_length()) as usize;
        let y = (pos.y / self.partition_side_length()) as usize;
        x + y * self.partition_count_per_side()
    }

    pub fn get_surrounding_partitions_indexes(&self, idx: usize) -> Vec<usize> {
        surrounding_grid_points(idx, self.partition_count_per_side(), self.periodic)
    }
}

fn periodic_vector_correction(mut r: Vec2, side_len: f64) -> Vec2 {
    if r.x >= 0.5 * side_len {
        r.x -= side_len;
    }
    if r.x < -0.5 * side_len {
        r.x += side_len;
    }

    if r.y >= 0.5 * side_len {
        r.y -= side_len;
    }
    if r.y < -0.5 * side_len {
        r.y += side_len;
    }

    r
}

fn get_larger_perfect_square(num: usize) -> usize {
    let next_n = (num as f64).sqrt().ceil() as usize;
    next_n
}

fn surrounding_grid_points(idx: usize, grid_side_count: usize, periodic: bool) -> Vec<usize> {
    let mut ret = Vec::with_capacity(9);
    let x = (idx % grid_side_count) as i64;
    let y = (idx / grid_side_count) as i64;
    for dx in -1..=1 {
        let mut new_x = x + dx;
        if periodic {
            new_x = (new_x + grid_side_count as i64) % grid_side_count as i64;
        } else {
            if new_x < 0 || new_x >= grid_side_count as i64 {
                continue;
            }
        }
        for dy in -1..=1 {
            let mut new_y = y + dy;
            if periodic {
                new_y = (new_y + grid_side_count as i64) % grid_side_count as i64;
            } else {
                if new_y < 0 || new_y >= grid_side_count as i64 {
                    continue;
                }
            }
            ret.push(new_x as usize + new_y as usize * grid_side_count)
        }
    }
    ret
}

fn get_points_on_square_grid(num: usize, side_len: f64, buffer: f64) -> Vec<Vec2> {
    let side_count = get_larger_perfect_square(num);
    let mut grid = Vec::with_capacity(num);

    let spacing = (side_len - 2. * buffer) / (side_count - 1) as f64;
    let mut x = buffer;
    let mut y = buffer;
    while grid.len() < num {
        grid.push(Vec2::at(x, y));
        x += spacing;
        if x > side_len - buffer {
            x = buffer;
            y += spacing;
        }
    }
    grid
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn larger_perfect_sqare() {
        assert_eq!(get_larger_perfect_square(25), 5);
        assert_eq!(get_larger_perfect_square(26), 6);
        assert_eq!(get_larger_perfect_square(35), 6);
        assert_eq!(get_larger_perfect_square(36), 6);
    }

    #[test]
    fn surrounding_points_non_periodic() {
        assert!(
            surrounding_grid_points(0, 10, false)
                .iter()
                .all(|i| [0, 1, 10, 11].contains(i)),
            "{:?} != {:?}",
            surrounding_grid_points(0, 10, false),
            [0, 1, 10, 11]
        );
        assert!(
            surrounding_grid_points(1, 10, false)
                .iter()
                .all(|i| [0, 1, 2, 10, 11, 12].contains(i)),
            "{:?} != {:?}",
            surrounding_grid_points(1, 10, false),
            [0, 1, 2, 10, 11, 12]
        );
        assert!(
            surrounding_grid_points(11, 10, false)
                .iter()
                .all(|i| [0, 1, 2, 10, 11, 12, 20, 21, 22].contains(i)),
            "{:?} != {:?}",
            surrounding_grid_points(11, 10, false),
            [0, 1, 2, 10, 11, 12, 20, 21, 22]
        );
        assert!(
            surrounding_grid_points(99, 10, false)
                .iter()
                .all(|i| [99, 98, 89, 88].contains(i)),
            "{:?} != {:?}",
            surrounding_grid_points(99, 10, false),
            [99, 98, 89, 88]
        );
    }

    #[test]
    fn surrounding_points_periodic() {
        assert!(
            surrounding_grid_points(0, 10, true)
                .iter()
                .all(|i| [0, 1, 10, 11, 99, 90, 9, 19, 91].contains(i)),
            "{:?} != {:?}",
            surrounding_grid_points(0, 10, true),
            [0, 1, 10, 11, 99, 90, 9, 19, 91]
        );
        assert!(
            surrounding_grid_points(1, 10, true)
                .iter()
                .all(|i| [0, 1, 2, 10, 11, 12, 90, 91, 92].contains(i)),
            "{:?} != {:?}",
            surrounding_grid_points(1, 10, true),
            [0, 1, 2, 10, 11, 12, 90, 91, 92]
        );
        assert!(
            surrounding_grid_points(11, 10, true)
                .iter()
                .all(|i| [0, 1, 2, 10, 11, 12, 20, 21, 22].contains(i)),
            "{:?} != {:?}",
            surrounding_grid_points(11, 10, true),
            [0, 1, 2, 10, 11, 12, 20, 21, 22]
        );
        assert!(
            surrounding_grid_points(99, 10, true)
                .iter()
                .all(|i| [99, 98, 89, 88, 0, 90, 80, 9, 8].contains(i)),
            "{:?} != {:?}",
            surrounding_grid_points(99, 10, true),
            [99, 98, 89, 88, 0, 90, 80, 9, 8]
        );
    }
}
