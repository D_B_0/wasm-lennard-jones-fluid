use std::fs::OpenOptions;
use std::io::Write;

use ljf_lib::Ensemble;

use clap::Parser;
use indicatif::{ProgressBar, ProgressStyle};
use string_builder::Builder;

#[derive(Debug, Parser)]
struct Args {
    /// File to save the results to
    #[clap(value_parser)]
    file_path: String,

    /// The number of particles to simulate
    #[clap(short, long, value_parser)]
    particle_count: usize,

    /// The ammount of time to increment by on each simulation step
    #[clap(short, long, value_parser)]
    dt: f64,

    /// The number of total steps to simulate
    #[clap(long, value_parser)]
    simulation_steps: usize,

    /// Every how many steps a new entry is recorded in the output file
    #[clap(short, long, value_parser, default_value_t = 1)]
    recoring_step_count: usize,

    /// The side length of the simulation box
    #[clap(long, value_parser, default_value_t = 10.0)]
    side_length: f64,

    /// The ammount of space to leave between the particle and the sides when instantiating
    /// [default: side_length * 0.075]
    #[clap(short, long, value_parser)]
    buffer: Option<f64>,

    /// The mass of a single particle
    #[clap(short, long, value_parser, default_value_t = 1.0)]
    mass: f64,

    /// Initial temperature
    #[clap(short, long, value_parser)]
    temperature: f64,

    /// The `sigma` parameter in the Lennard Jones potential
    #[clap(short, long, value_parser, default_value_t = 1.0)]
    sigma: f64,

    /// The `epsilon` parameter in the Lennard Jones potential
    #[clap(short, long, value_parser, default_value_t = 1.0)]
    epsilon: f64,
}

fn main() {
    let args = Args::parse();

    let file = OpenOptions::new()
        .write(true)
        .append(false)
        .create(true)
        .open(args.file_path);

    if let Err(e) = file {
        println!("error: the specified file could not be opened");
        println!("{}", e.to_string());
        std::process::exit(1);
    }

    let mut file = file.unwrap();

    {
        use std::time::{SystemTime, UNIX_EPOCH};
        ljf_lib::rand::xorshiro256::seed(
            SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .expect("Error while getting system time")
                .as_nanos() as u64,
        );
    }

    let mut ensemble = Ensemble::new(
        args.particle_count,
        args.side_length,
        args.buffer.unwrap_or(args.side_length * 0.075),
        args.mass,
        args.temperature,
    );
    ensemble.sigma = args.sigma;
    ensemble.epsilon = args.epsilon;

    let mut file_buf = Builder::default();
    file_buf.append("t");
    for i in 0..ensemble.n_particles {
        file_buf.append(format!(",p{}_r_x", i));
        file_buf.append(format!(",p{}_r_y", i));
        file_buf.append(format!(",p{}_v_x", i));
        file_buf.append(format!(",p{}_v_y", i));
    }
    file_buf.append("\n");

    let prog_bar = ProgressBar::new(args.simulation_steps as u64);
    prog_bar.set_style(
        ProgressStyle::with_template("[{wide_bar:.white/dim}] {pos}/{len} ({eta})")
            .unwrap()
            .progress_chars("==_"),
    );
    for i in 0..args.simulation_steps {
        ensemble.step(args.dt);
        if i % args.recoring_step_count == 0 {
            // save to file
            file_buf.append(format!("{}", args.dt * i as f64));
            for p in &ensemble.particles {
                file_buf.append(format!(",{}", p.r.x));
                file_buf.append(format!(",{}", p.r.y));
                file_buf.append(format!(",{}", p.v.x));
                file_buf.append(format!(",{}", p.v.y));
            }
            file_buf.append("\n");
            if file_buf.len() > 1000 {
                if let Err(e) = file.write(file_buf.string().unwrap().as_bytes()) {
                    println!("error: could not write to the file");
                    println!("{}", e.to_string());
                    std::process::exit(1);
                }
                file_buf = Builder::default();
            }
        }
        prog_bar.inc(1);
    }
    prog_bar.finish_with_message("Finished");

    if let Err(e) = file.write(file_buf.string().unwrap().as_bytes()) {
        println!("error: could not write to the file");
        println!("{}", e.to_string());
        std::process::exit(1);
    }
}
