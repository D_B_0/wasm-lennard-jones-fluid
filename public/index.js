// TODO: find a way to automatically change this when deploying
import { createApp } from "https://unpkg.com/vue@3/dist/vue.esm-browser.prod.js";
// import { createApp } from "https://unpkg.com/vue@3/dist/vue.esm-browser.js";

import * as wasm from "./ljf-wasm.js";

createApp({
  data() {
    return {
      ctx: null,
      wasm_loaded: false,
      wasm_obj: false,
      ensemble: null,
      callback_id: null,
      last_callback_timestamp: null,
      fps_array: [],
      runtime: {
        play: false,
        draw_velocities: false,
        draw_bonds: true,
        steps_per_animation_frame: 5,
        physics_time_step: 0.005,
      },
      raw_physics_time_step: 0.005,
      velocity_draw_scale: 50,
      init: {
        n_particles: 25,
        side_len: 10,
        mass: 1,
        temperature: 1,
        sigma: 1,
        epsilon: 1,
      },
    }
  },
  mounted() {
    this.ctx = this.$refs.canvas.getContext("2d");
    wasm.default().then(wasm_obj => {
      this.wasm_loaded = true;
      this.wasm_obj = wasm_obj;
      this.callback_id = window.requestAnimationFrame(this.animate);
    });
  },
  watch: {
    raw_physics_time_step: debounce(function (newVal) {
      this.runtime.physics_time_step = newVal;
    }, 500),
  },
  methods: {
    stop() {
      this.ensemble = null;
      this.float_view = null;
      this.runtime.play = false;
    },
    generate_ensemble() {
      this.ensemble = wasm.Ensemble.new(this.init.n_particles, this.init.side_len, this.init.side_len * 0.075, this.init.mass, this.init.temperature);
      this.ensemble.set_sigma(this.init.sigma);
      this.ensemble.set_epsilon(this.init.epsilon);
      this.ensemble.set_periodic(false);

      this.runtime.play = false;
    },
    animate() {
      const { width, height } = this.$refs.canvas;

      const current_timestamp = new Date();
      if (this.last_callback_timestamp != null) {
        this.fps_array.push(1 / ((current_timestamp - this.last_callback_timestamp) / 1000));
        if (this.fps_array.length > 10) {
          this.fps_array.splice(0, 1);
        }
      }

      this.ctx.fillStyle = "white";
      this.ctx.fillRect(0, 0, width, height);

      if (this.ensemble == null) {
        this.ctx.fillStyle = "black";
        this.ctx.textBaseline = "middle";
        this.ctx.textAlign = "center"
        this.ctx.font = "40px sans-serif";
        this.ctx.fillText("Click \"Generate\" and then \"Play\"", width / 2, height / 2);

        this.callback_id = window.requestAnimationFrame(this.animate);
        return;
      }

      let res = this.ensemble.draw(this.$refs.canvas, this.ctx, {
        draw_scale: this.draw_scale,
        draw_bonds: this.runtime.draw_bonds,
        draw_velocities: this.runtime.draw_velocities,
        velocity_draw_scale: this.velocity_draw_scale,
        fps: this.fps_array.length > 0 ? this.fps_array.reduce((acc, val) => acc + val) / this.fps_array.length : 0,
      });
      if (res != undefined) {
        console.error(res);
      }

      if (this.runtime.play) {
        for (let i = 0; i < this.runtime.steps_per_animation_frame; i++) {
          this.ensemble.step(this.runtime.physics_time_step);
        }
      }

      this.last_callback_timestamp = current_timestamp;
      this.callback_id = window.requestAnimationFrame(this.animate);
    }
  },
  computed: {
    draw_scale() {
      return this.$refs.canvas.width / this.ensemble.side_len();
    },
  }
}).mount('#app')

function debounce(fn, delay) {
  let timeoutID = null;
  return function (...args) {
    clearTimeout(timeoutID);
    let that = this;
    timeoutID = setTimeout(function () {
      fn.apply(that, args);
    }, delay);
  }
}
