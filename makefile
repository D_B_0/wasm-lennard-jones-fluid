ifeq ($(OS), Windows_NT)
	CP = copy /y
	SEP = \\
else
	CP = cp
	SEP = /
endif

WASM_PKG_NAME = ljf-wasm

.PHONY: all
all: webapp cli

.PHONY: webapp
webapp: wasm
	$(CP) .$(SEP)pkg$(SEP)$(WASM_PKG_NAME)_bg.wasm .$(SEP)public
	$(CP) .$(SEP)pkg$(SEP)$(WASM_PKG_NAME).js .$(SEP)public

.PHONY: wasm
wasm: ljf-wasm/* ljf-lib/*
	wasm-pack build ljf-wasm --target web --out-dir ../pkg --out-name $(WASM_PKG_NAME)

.PHONY: cli
cli: ljf-cli/* ljf-lib/*
	cargo build -p ljf-cli --release

.PHONY: test
test:
	cargo test -- --test-threads=1